use advent_of_code_2020::{read_puzzle_input, ParseList};
use std::error::Error;
use std::cmp::Ordering;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(9)?;
    let numbers = u64::parse_list(&input)?;
    let not_previous_sum = puzzle_one(&numbers, 25);

    println!("{:?}", not_previous_sum);

    let weakness = puzzle_two(&numbers, 25);

    println!("{:?}", weakness);

    Ok(())
}

fn puzzle_one(numbers: &[u64], preamble: usize) -> Option<u64> {
    let mut slice = vec![0; preamble];
    'number_loop: for (index, &number) in numbers.iter().enumerate().skip(preamble) {
        slice.clone_from_slice(&numbers[index - preamble..index]);
        slice.sort_unstable();

        for (left_index, &left) in slice.iter().enumerate() {
            if left > number {
                break;
            }

            for &right in slice.iter().skip(left_index) {
                match (left + right).cmp(&number) {
                    Ordering::Equal => continue 'number_loop,
                    Ordering::Greater => break,
                    Ordering::Less => {}
                }
            }
        }

        return Some(number);
    }

    None
}

fn puzzle_two(numbers: &[u64], preamble: usize) -> Option<u64> {
    let invalid_number = puzzle_one(numbers, preamble)?;

    for start_index in 0..numbers.len() {
        for end_index in start_index + 1..numbers.len() {
            let range = &numbers[start_index..=end_index];
            let sum: u64 = range.iter().sum();

            match sum.cmp(&invalid_number) {
                Ordering::Equal => {
                    let min = range.iter().min().unwrap();
                    let max = range.iter().max().unwrap();
                    return Some(*min + *max);
                }
                Ordering::Greater => break,
                Ordering::Less => {}
            }
        }
    }

    None
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two};
    use advent_of_code_2020::ParseList;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_9_example")?;
        let numbers = u64::parse_list(&input)?;
        let not_previous_sum = puzzle_one(&numbers, 5);

        assert_eq!(Some(127), not_previous_sum);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_9_example")?;
        let numbers = u64::parse_list(&input)?;
        let weakness = puzzle_two(&numbers, 5);

        assert_eq!(Some(62), weakness);

        Ok(())
    }
}
