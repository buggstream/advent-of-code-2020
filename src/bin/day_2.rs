use advent_of_code_2020::read_puzzle_input;
use parse_display::{Display as PDisplay, FromStr as PFromStr};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(2)?;
    let password_rules = parse_password_rules(&input)?;
    let valid_passwords = puzzle_one(&password_rules);

    println!("{}", valid_passwords);

    let new_rules = puzzle_two(&password_rules);

    println!("{}", new_rules);

    Ok(())
}

fn puzzle_one(rules: &[PasswordRule]) -> u32 {
    rules
        .iter()
        .filter(|rule| {
            let occurs = rule.password.matches(rule.letter).count() as u32;
            occurs >= rule.min && occurs <= rule.max
        })
        .count() as u32
}

fn puzzle_two(rules: &[PasswordRule]) -> u32 {
    rules
        .iter()
        .filter(|rule| {
            let first_pos = rule.password.chars().nth((rule.min - 1) as usize);
            let second_pos = rule.password.chars().nth((rule.max - 1) as usize);

            (first_pos == Some(rule.letter)) != (second_pos == Some(rule.letter))
        })
        .count() as u32
}

fn parse_password_rules(input: &str) -> Result<Vec<PasswordRule>, impl Error> {
    input
        .lines()
        .map(|line| line.parse::<PasswordRule>())
        .collect()
}

#[derive(PDisplay, PFromStr, Debug)]
#[display("{min}-{max} {letter}: {password}")]
struct PasswordRule {
    min: u32,
    max: u32,
    letter: char,
    password: String,
}
