use advent_of_code_2020::{read_puzzle_input, ParseList};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(10)?;
    let numbers = i64::parse_list(&input)?;
    let jolt_differences = puzzle_one(&numbers);

    println!("{:?}", jolt_differences);

    let configurations = puzzle_two(&numbers);

    println!("{:?}", configurations);

    Ok(())
}

fn puzzle_one(adaptors: &[i64]) -> u64 {
    let mut adaptors = Vec::from(adaptors);
    adaptors.sort_unstable();
    adaptors.insert(0, 0);
    adaptors.insert(adaptors.len(), adaptors.last().unwrap() + 3);
    let (ones, threes) = adaptors.windows(2).fold((0, 0), |(ones, threes), window| {
        match window[1] - window[0] {
            1 => (ones + 1, threes),
            3 => (ones, threes + 1),
            _ => (ones, threes),
        }
    });

    ones * threes
}

fn puzzle_two(adaptors: &[i64]) -> u64 {
    let mut adaptors = Vec::from(adaptors);
    adaptors.sort_unstable();
    adaptors.insert(0, 0);
    adaptors.insert(adaptors.len(), adaptors.last().unwrap() + 3);

    let mut memory = vec![0; adaptors.len()];
    memory[adaptors.len() - 1] = 1_u64;

    for index in (0..adaptors.len() - 1).rev() {
        let configurations = (index + 1..adaptors.len())
            .take_while(|next_index| adaptors[*next_index] - adaptors[index] <= 3)
            .map(|next_index| memory[next_index])
            .sum();

        memory[index] = configurations;
    }

    memory[0]
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two};
    use advent_of_code_2020::ParseList;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_10_example")?;
        let numbers = i64::parse_list(&input)?;
        let jolt_differences = puzzle_one(&numbers);

        assert_eq!(35, jolt_differences);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_10_example")?;
        let numbers = i64::parse_list(&input)?;
        let jolt_differences = puzzle_two(&numbers);

        assert_eq!(8, jolt_differences);

        Ok(())
    }

    #[test]
    fn puzzle_two_example_long() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_10_example_long")?;
        let numbers = i64::parse_list(&input)?;
        let jolt_differences = puzzle_two(&numbers);

        assert_eq!(19208, jolt_differences);

        Ok(())
    }
}
