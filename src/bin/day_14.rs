use advent_of_code_2020::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;

lazy_static! {
    static ref INSTRUCTIONS: Regex =
        Regex::new(r"(?m)^mask = (?P<mask>[01X]{36})|mem\[(?P<pointer>\d+)\] = (?P<value>\d+)$")
            .unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(14)?;
    let instructions = parse_instructions(&input)?;
    let memory_sum = puzzle_one(&instructions);

    println!("sum: {}", memory_sum);

    let memory_sum_v2 = puzzle_two(&instructions);

    println!("sum_v2: {}", memory_sum_v2);

    Ok(())
}

fn puzzle_one(instructions: &[BitmaskInstruction]) -> u64 {
    let mut override_mask = 0;
    let mut remain_mask = 0;
    let mut memory = HashMap::new();

    for instruction in instructions {
        match instruction {
            BitmaskInstruction::Mask(new_override_mask, new_remain_mask) => {
                override_mask = *new_override_mask;
                remain_mask = *new_remain_mask;
            }
            BitmaskInstruction::Assign(pointer, value) => {
                let memory_location = memory.entry(*pointer).or_insert(0);
                *memory_location = (*value & remain_mask) | override_mask;
            }
        }
    }

    memory.values().sum()
}

fn puzzle_two(instructions: &[BitmaskInstruction]) -> u64 {
    let mut base_mask = 0;
    let mut mask_permutations = Vec::new();
    let mut memory = HashMap::new();

    for instruction in instructions {
        match instruction {
            BitmaskInstruction::Mask(new_base_mask, floating_mask) => {
                base_mask = *new_base_mask;
                mask_permutations.clear();
                mask_permutations.push(0);

                // 36 bits at most
                for index in 0..36_usize {
                    if floating_mask & (1 << index) > 0 {
                        let new_masks: Vec<_> = mask_permutations
                            .iter()
                            .map(|mask| *mask | (1 << index))
                            .collect();
                        mask_permutations.extend_from_slice(&new_masks);
                    }
                }
            }
            BitmaskInstruction::Assign(pointer, value) => {
                for floating_mask in mask_permutations.iter().copied() {
                    let masked_pointer = (floating_mask ^ *pointer) | base_mask;
                    memory.insert(masked_pointer, *value);
                }
            }
        }
    }

    memory.values().sum()
}

fn parse_instructions(input: &str) -> Result<Vec<BitmaskInstruction>, Box<dyn Error>> {
    let mut instructions = Vec::new();

    for captures in INSTRUCTIONS.captures_iter(&input) {
        match (
            captures.name("mask"),
            captures.name("pointer"),
            captures.name("value"),
        ) {
            (Some(mask), None, None) => {
                let (base_mask, floating_mask) = parse_mask(mask.as_str());
                instructions.push(BitmaskInstruction::Mask(base_mask, floating_mask));
            }
            (None, Some(pointer_match), Some(value_match)) => {
                let pointer = pointer_match.as_str().parse::<u64>()?;
                let value = value_match.as_str().parse::<u64>()?;

                instructions.push(BitmaskInstruction::Assign(pointer, value));
            }
            _ => {} // Skip invalid instructions
        }
    }

    Ok(instructions)
}

fn parse_mask(mask: &str) -> (u64, u64) {
    let mut base_mask = 0;
    let mut floating_mask = 0;

    for (index, floating_bit) in mask.chars().rev().enumerate() {
        match floating_bit {
            'X' => floating_mask |= 1 << index,
            '1' => base_mask |= 1 << index,
            '0' => {} // Mask is already zero at this position.
            _ => unreachable!(
                "The instruction parsing regex should prevent this from being matched."
            ),
        }
    }

    (base_mask, floating_mask)
}

#[derive(Debug, Copy, Clone)]
enum BitmaskInstruction {
    Mask(u64, u64),
    Assign(u64, u64),
}

#[cfg(test)]
mod test {
    use crate::{parse_instructions, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_14_small")?;
        let instructions = parse_instructions(&input)?;
        let memory_sum = puzzle_one(&instructions);

        assert_eq!(165, memory_sum);

        Ok(())
    }

    #[test]
    fn puzzle_two_medium() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_14_medium")?;
        let instructions = parse_instructions(&input)?;
        let memory_sum = puzzle_two(&instructions);

        assert_eq!(208, memory_sum);

        Ok(())
    }

    #[test]
    fn puzzle_two_custom() -> Result<(), Box<dyn Error>> {
        let location = 0b101010_u64;
        let input = format!(
            "mask = 000000000000000000000000000000XX1100\nmem[{}] = 1",
            location
        );
        let instructions = parse_instructions(&input)?;
        let memory_sum = puzzle_two(&instructions);

        assert_eq!(4, memory_sum);

        Ok(())
    }
}
