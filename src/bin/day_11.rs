use advent_of_code_2020::read_puzzle_input;
use ndarray::Array2;
use std::cmp::min;
use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(11)?;
    let seats = input.parse::<SeatArea>()?;
    let occupied_seats = puzzle_one(seats.clone());

    println!("{}", occupied_seats);

    let occupied_seats_directions = puzzle_two(seats);

    println!("{}", occupied_seats_directions);

    Ok(())
}

const DIRECTIONS: [(i64, i64); 8] = [
    (0, 1),
    (0, -1),
    (1, 0),
    (-1, 0),
    (1, 1),
    (1, -1),
    (-1, -1),
    (-1, 1),
];

fn puzzle_one(seats: SeatArea) -> usize {
    seat_simulation(seats, neighbour_seat_update)
}

fn puzzle_two(seats: SeatArea) -> usize {
    seat_simulation(seats, direction_seat_update)
}

fn neighbour_seat_update(seats: &SeatArea, (x, y): (i64, i64)) -> Option<SeatType> {
    let occupied_neighbours = DIRECTIONS
        .iter()
        .filter_map(|(x_dir, y_dir)| seats.get((x + *x_dir, y + *y_dir)))
        .filter(|seat| **seat == SeatType::OccupiedSeat)
        .count();

    match seats.get((x, y)).unwrap() {
        SeatType::EmptySeat if occupied_neighbours == 0 => Some(SeatType::OccupiedSeat),
        SeatType::OccupiedSeat if occupied_neighbours >= 4 => Some(SeatType::EmptySeat),
        _ => None,
    }
}

fn direction_seat_update(seats: &SeatArea, (x, y): (i64, i64)) -> Option<SeatType> {
    let mut occupied_neighbours = 0_u32;
    for direction in DIRECTIONS.iter() {
        let (x_dir, y_dir) = *direction;
        let (mut check_x, mut check_y) = (x, y);

        loop {
            check_x += x_dir;
            check_y += y_dir;

            if let Some(SeatType::OccupiedSeat) = seats.get((check_x, check_y)) {
                occupied_neighbours += 1;
            }

            if Some(&SeatType::Floor) != seats.get((check_x, check_y)) {
                break;
            }
        }
    }

    match seats.get((x, y)).unwrap() {
        SeatType::EmptySeat if occupied_neighbours == 0 => Some(SeatType::OccupiedSeat),
        SeatType::OccupiedSeat if occupied_neighbours >= 5 => Some(SeatType::EmptySeat),
        _ => None,
    }
}

fn seat_simulation(
    mut seats: SeatArea,
    seat_update: impl Fn(&SeatArea, (i64, i64)) -> Option<SeatType>,
) -> usize {
    let mut updated = true;

    while updated {
        updated = false;
        let mut updated_seats = seats.clone();

        for y in 0..seats.height() as i64 {
            for x in 0..seats.width() as i64 {
                if let Some(updated_seat_type) = seat_update(&seats, (x, y)) {
                    let seat = updated_seats.get_mut((x, y)).unwrap();
                    *seat = updated_seat_type;
                    updated = true;
                }
            }
        }

        seats = updated_seats;
    }

    seats.count_occupied_seats()
}

#[derive(Debug, Clone, PartialEq)]
struct SeatArea {
    seats: Array2<SeatType>,
}

impl SeatArea {
    fn get(&self, (x, y): (i64, i64)) -> Option<&SeatType> {
        let (x_index, y_index) = (usize::try_from(x).ok()?, usize::try_from(y).ok()?);

        self.seats.get((y_index, x_index))
    }

    fn get_mut(&mut self, (x, y): (i64, i64)) -> Option<&mut SeatType> {
        let (x_index, y_index) = (usize::try_from(x).ok()?, usize::try_from(y).ok()?);

        self.seats.get_mut((y_index, x_index))
    }

    fn count_occupied_seats(&self) -> usize {
        self.seats
            .iter()
            .filter(|seat| **seat == SeatType::OccupiedSeat)
            .count()
    }

    fn height(&self) -> usize {
        self.seats.nrows()
    }

    fn width(&self) -> usize {
        self.seats.ncols()
    }
}

impl FromStr for SeatArea {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut iter = input.lines().map(|line| line.chars().count());
        let width = iter.next().unwrap_or(0);
        let height = iter.count() + min(1, width);

        let parsed_elements = input
            .lines()
            .flat_map(|line| line.chars())
            .map(|letter| letter.try_into())
            .collect::<Result<Vec<SeatType>, _>>()?;
        let seats = Array2::from_shape_vec((height, width), parsed_elements)?;
        Ok(SeatArea { seats })
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum SeatType {
    Floor,
    EmptySeat,
    OccupiedSeat,
}

impl TryFrom<char> for SeatType {
    type Error = Box<dyn Error>;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(SeatType::Floor),
            'L' => Ok(SeatType::EmptySeat),
            '#' => Ok(SeatType::OccupiedSeat),
            _ => Err("Invalid seat character!")?,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two, SeatArea};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_11_small")?;
        let seats = input.parse::<SeatArea>()?;
        let occupied_seats = puzzle_one(seats);

        assert_eq!(37, occupied_seats);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_11_small")?;
        let seats = input.parse::<SeatArea>()?;
        let occupied_seats = puzzle_two(seats);

        assert_eq!(26, occupied_seats);

        Ok(())
    }
}
