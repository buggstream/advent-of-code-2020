use advent_of_code_2020::read_puzzle_input;
use parse_display::{Display as PDisplay, FromStr as PFromStr};
use std::collections::HashSet;
use std::convert::TryFrom;
use std::error::Error;
use std::mem::swap;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(8)?;
    let instructions = parse_instructions(&input)?;
    let accumulator = puzzle_one(&instructions);

    println!("{:?}", accumulator);

    let fixed_accumulator = puzzle_two(&instructions);

    println!("{:?}", fixed_accumulator);

    Ok(())
}

fn puzzle_one(instructions: &[Instruction]) -> Terminate {
    let mut executed = HashSet::new();
    let mut acc = 0;
    let mut program_pointer = 0;

    loop {
        if !executed.insert(program_pointer) {
            return Terminate::Loop(acc);
        }

        let index = match usize::try_from(program_pointer) {
            Ok(index) => index,
            Err(_) => return Terminate::InvalidCounter,
        };

        let instruction = match instructions.get(index) {
            Some(instruction) => instruction,
            None => break,
        };

        match *instruction {
            Instruction::Acc(offset) => {
                acc += offset;
                program_pointer += 1;
            }
            Instruction::Jmp(offset) => {
                program_pointer += offset;
            }
            Instruction::Nop(_) => {
                program_pointer += 1;
            }
        }
    }

    Terminate::Normal(acc)
}

fn puzzle_two(instructions: &[Instruction]) -> Option<i64> {
    let mut modified: Vec<_> = instructions.iter().copied().collect();

    for (index, instruction) in instructions.iter().enumerate() {
        let mut swapped_instruction = match *instruction {
            Instruction::Nop(offset) => Instruction::Jmp(offset),
            Instruction::Jmp(offset) => Instruction::Nop(offset),
            Instruction::Acc(_) => continue,
        };

        swap(&mut swapped_instruction, &mut modified[index]);
        if let Terminate::Normal(acc) = puzzle_one(&modified) {
            return Some(acc);
        }
        swap(&mut swapped_instruction, &mut modified[index]);
    }

    None
}

fn parse_instructions(input: &str) -> Result<Vec<Instruction>, impl Error> {
    input
        .lines()
        .map(|line| line.parse::<Instruction>())
        .collect()
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Terminate {
    Loop(i64),
    Normal(i64),
    InvalidCounter,
}

#[derive(PDisplay, PFromStr, Debug, Copy, Clone)]
enum Instruction {
    #[display("acc {0}")]
    Acc(i64),
    #[display("jmp {0}")]
    Jmp(i64),
    #[display("nop {0}")]
    Nop(i64),
}

#[cfg(test)]
mod test {
    use crate::{parse_instructions, puzzle_one, puzzle_two, Terminate};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_8_example")?;
        let instructions = parse_instructions(&input)?;
        let accumulator = puzzle_one(&instructions);

        assert_eq!(Terminate::Loop(5), accumulator);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_8_example")?;
        let instructions = parse_instructions(&input)?;
        let accumulator = puzzle_two(&instructions);

        assert_eq!(Some(8), accumulator);

        Ok(())
    }
}
