use advent_of_code_2020::read_puzzle_input;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(5)?;
    let max_id = puzzle_one(&input);

    println!("Max id: {:?}", max_id);

    let my_seat = puzzle_two(&input);
    println!("My seat: {:?}", my_seat);

    Ok(())
}

fn puzzle_one(input: &str) -> Option<u32> {
    get_ids(input)?.into_iter().max()
}

fn puzzle_two(input: &str) -> Option<u32> {
    let mut ids = get_ids(input)?;
    ids.sort();

    let mut iter = ids.into_iter();
    let mut previous = iter.next()?;

    while let Some(current) = iter.next() {
        if previous + 1 != current {
            return Some(previous + 1);
        }

        previous = current;
    }

    None
}

fn get_ids(input: &str) -> Option<Vec<u32>> {
    let mut ids = Vec::new();

    for line in input.lines() {
        let (mut lower, mut upper, mut left, mut right) = (0, 127, 0, 7);
        let mut chars = line.chars();

        let (x, y) = loop {
            let (range_x, range_y) = (right - left, upper - lower);
            if range_x == 0 && range_y == 0 {
                break (lower, left);
            }

            let character = match chars.next() {
                Some(character) => character,
                None => return None,
            };

            match character {
                'F' => upper = lower + (range_y / 2),
                'B' => lower = lower + (range_y / 2) + 1,
                'L' => right = left + (range_x / 2),
                'R' => left = left + (range_x / 2) + 1,
                _ => unreachable!(),
            }
        };

        ids.push(x * 8 + y);
    }

    Some(ids)
}

#[cfg(test)]
mod test {
    use crate::puzzle_one;
    use std::error::Error;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = "FBFBBFFRLR";
        let max_id = puzzle_one(input);

        assert_eq!(Some(357), max_id);

        Ok(())
    }
}
