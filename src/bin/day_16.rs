use advent_of_code_2020::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::ops::RangeInclusive;

type Ticket = Vec<u32>;
type TicketFields<'a> = Vec<(&'a str, [RangeInclusive<u32>; 2])>;

lazy_static! {
    static ref TICKETFIELD: Regex =
        Regex::new(r"(?m)^([[[:alpha:]] ]+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(16)?;
    let (ticket_fields, your_ticket, nearby_tickets) = parse_tickets(&input);
    let error_rate = puzzle_one(&ticket_fields, &nearby_tickets);

    println!("{}", error_rate);

    let departure_hash = puzzle_two(&ticket_fields, &your_ticket, &nearby_tickets);

    println!("{:?}", departure_hash);

    Ok(())
}

fn puzzle_one(ticket_fields: &TicketFields, nearby_tickets: &[Ticket]) -> u32 {
    let valid_ranges = ticket_field_ranges(ticket_fields);

    nearby_tickets
        .iter()
        .flatten()
        .copied()
        .filter(|field_value| !valid_ranges.iter().any(|range| range.contains(field_value)))
        .sum()
}

fn puzzle_two(
    ticket_fields: &TicketFields,
    your_ticket: &Ticket,
    nearby_tickets: &[Ticket],
) -> Option<u64> {
    let valid_ranges = ticket_field_ranges(ticket_fields);
    let valid_tickets: Vec<_> = nearby_tickets
        .iter()
        .filter(|ticket| {
            ticket
                .iter()
                .all(|field_value| valid_ranges.iter().any(|range| range.contains(field_value)))
        })
        .collect();

    let mut possibilities = HashMap::new();

    for (field_name, ranges) in ticket_fields {
        for index in 0..ticket_fields.len() {
            let all_within_range = valid_tickets
                .iter()
                .map(|ticket| ticket[index])
                .all(|field_value| ranges.iter().any(|range| range.contains(&field_value)));

            if all_within_range {
                let valid_fields = possibilities.entry(*field_name).or_insert(HashSet::new());
                valid_fields.insert(index);
            }
        }
    }

    let mut locked_options = HashMap::new();

    loop {
        let only_option = possibilities
            .iter()
            .find_map(|(field_name, field_indices)| {
                if field_indices.len() == 1 {
                    Some((*field_name, field_indices.iter().next().unwrap()))
                } else {
                    None
                }
            });

        let (name, &field_index) = match only_option {
            Some(option_value) => option_value,
            None => break,
        };

        locked_options.insert(field_index, name);

        // Remove locked options
        for (_, possibility_set) in possibilities.iter_mut() {
            possibility_set.remove(&field_index);
        }
    }

    Some(
        locked_options
            .iter()
            .filter(|(_, name)| name.starts_with("departure "))
            .map(|(index, _)| your_ticket[*index] as u64)
            .product(),
    )
}

fn parse_tickets(input: &str) -> (TicketFields, Ticket, Vec<Ticket>) {
    let your_ticket_index = input.find("your ticket:").unwrap();
    let nearby_tickets = input.find("nearby tickets:").unwrap();

    let fields_str = input.get(..your_ticket_index).unwrap();
    let your_ticket_str = input.get(your_ticket_index..nearby_tickets).unwrap();
    let nearby_tickets_str = input.get(nearby_tickets..).unwrap();

    let mut fields = Vec::new();
    for captures in TICKETFIELD.captures_iter(fields_str) {
        let ticket_name = captures.get(1).unwrap().as_str();
        let first_lower = captures.get(2).unwrap().as_str().parse::<u32>().unwrap();
        let first_upper = captures.get(3).unwrap().as_str().parse::<u32>().unwrap();
        let second_lower = captures.get(4).unwrap().as_str().parse::<u32>().unwrap();
        let second_upper = captures.get(5).unwrap().as_str().parse::<u32>().unwrap();

        let bounds = [first_lower..=first_upper, second_lower..=second_upper];

        fields.push((ticket_name, bounds));
    }

    let your_ticket = your_ticket_str
        .lines()
        .skip(1)
        .flat_map(|line| {
            line.split(',')
                .filter_map(|number_str| number_str.parse::<u32>().ok())
        })
        .collect();

    let nearby_tickets = nearby_tickets_str
        .lines()
        .skip(1)
        .map(|line| {
            line.split(',')
                .filter_map(|number_str| number_str.parse::<u32>().ok())
                .collect()
        })
        .collect();

    (fields, your_ticket, nearby_tickets)
}

fn ticket_field_ranges(ticket_fields: &TicketFields) -> Vec<RangeInclusive<u32>> {
    ticket_fields
        .iter()
        .flat_map(|(_, ranges)| ranges)
        .cloned()
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{parse_tickets, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_16_small")?;
        let (ticket_fields, _, nearby_tickets) = parse_tickets(&input);
        let error_rate = puzzle_one(&ticket_fields, &nearby_tickets);

        assert_eq!(71, error_rate);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_16_medium")?;
        let (ticket_fields, your_ticket, nearby_tickets) = parse_tickets(&input);
        let departure_hash = puzzle_two(&ticket_fields, &your_ticket, &nearby_tickets);

        assert_eq!(Some(1), departure_hash);

        Ok(())
    }
}
