use advent_of_code_2020::read_puzzle_input;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(13)?;
    let (start_timestamp, busses) = parse_schedule(&input).unwrap();
    let earliest_bus_time = puzzle_one(start_timestamp, &busses);

    println!("{}", earliest_bus_time);

    let t = puzzle_two(&busses);
    println!("t: {}", t);

    Ok(())
}

fn puzzle_one(start_timestamp: u64, busses: &[(u64, u64)]) -> u64 {
    busses
        .iter()
        .copied()
        .map(|(_, bus)| (bus, bus - (start_timestamp % bus)))
        .min_by_key(|(_, waiting_time)| *waiting_time)
        .map(|(bus, waiting_time)| bus * waiting_time)
        .unwrap_or(0)
}

fn puzzle_two(busses: &[(u64, u64)]) -> u64 {
    let mut step_size = 1;
    let mut t = 0;

    for (bus_offset, bus_id) in busses.iter().copied() {
        while (t + bus_offset) % bus_id != 0 {
            t += step_size;
        }

        step_size *= bus_id;
    }

    t
}

fn parse_schedule(input: &str) -> Option<(u64, Vec<(u64, u64)>)> {
    let mut lines = input.lines();
    let start_timestamp = lines.next()?.parse::<u64>().ok()?;

    let busses: Vec<_> = lines
        .next()?
        .split(',')
        .enumerate()
        .filter_map(|(index, bus_str)| bus_str.parse::<u64>().ok().map(|bus| (index as u64, bus)))
        .collect();

    Some((start_timestamp, busses))
}

#[cfg(test)]
mod test {
    use crate::{parse_schedule, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_13_small")?;
        let (start_timestamp, busses) = parse_schedule(&input).unwrap();
        let earliest_bus_time = puzzle_one(start_timestamp, &busses);

        assert_eq!(295, earliest_bus_time);

        Ok(())
    }

    #[test]
    fn puzzle_two_basic_1() -> Result<(), Box<dyn Error>> {
        let input = "0\n3,x,4,5";
        let (_, busses) = parse_schedule(input).unwrap();
        let t = puzzle_two(&busses);

        assert_eq!(42, t);

        Ok(())
    }

    #[test]
    fn puzzle_two_basic_2() -> Result<(), Box<dyn Error>> {
        let input = "0\n17,x,13,19";
        let (_, busses) = parse_schedule(input).unwrap();
        let t = puzzle_two(&busses);

        assert_eq!(3417, t);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_13_small")?;
        let (_, busses) = parse_schedule(&input).unwrap();
        let t = puzzle_two(&busses);

        assert_eq!(1068781, t);

        Ok(())
    }
}
