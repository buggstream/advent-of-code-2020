use advent_of_code_2020::read_puzzle_input;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(1)?;
    let parsed: Result<Vec<_>, _> = input.lines().map(|line| line.parse::<u64>()).collect();
    let numbers = parsed?;

    let sum_of_two = puzzle_one(&numbers);
    println!("{:?}", sum_of_two);

    let sum_of_three = puzzle_two(&numbers);
    println!("{:?}", sum_of_three);

    Ok(())
}

fn puzzle_one(numbers: &[u64]) -> Option<u64> {
    for (index, first) in numbers.iter().cloned().enumerate() {
        for second in numbers.iter().skip(index).cloned() {
            if first + second == 2020 {
                return Some(first * second);
            }
        }
    }

    None
}

fn puzzle_two(numbers: &[u64]) -> Option<u64> {
    for (first_index, first) in numbers.iter().cloned().enumerate() {
        for (second_index, second) in numbers.iter().cloned().enumerate().skip(first_index) {
            if first + second >= 2020 {
                continue;
            }

            for third in numbers.iter().cloned().skip(second_index) {
                if first + second + third == 2020 {
                    return Some(first * second * third);
                }
            }
        }
    }

    None
}
