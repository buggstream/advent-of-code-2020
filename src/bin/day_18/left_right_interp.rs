use crate::MathToken;
use std::collections::VecDeque;
use std::error::Error;
use std::iter::FromIterator;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum InterpState {
    Start,
    Acc(i64),
    Adding(i64),
    Multiplying(i64),
}

pub fn interp(tokens: &[MathToken]) -> Result<i64, Box<dyn Error>> {
    let mut token_stack = VecDeque::from_iter(tokens.iter().copied().rev());
    let mut state_stack = VecDeque::new();
    let mut state = InterpState::Start;

    while let Some(next_token) = token_stack.pop_back() {
        state = match (state, next_token) {
            (InterpState::Start, MathToken::Number(number)) => InterpState::Acc(number),
            (InterpState::Start, MathToken::OpenParen) => {
                state_stack.push_back(InterpState::Start);
                InterpState::Start
            }
            (InterpState::Acc(acc), MathToken::Add) => InterpState::Adding(acc),
            (InterpState::Acc(acc), MathToken::Multiply) => InterpState::Multiplying(acc),
            (InterpState::Acc(acc), MathToken::CloseParen) => {
                token_stack.push_back(MathToken::Number(acc));

                match state_stack.pop_back() {
                    Some(prev_state) => prev_state,
                    None => {
                        return Err(
                            "Syntax error! Closing parentheses without opening parentheses".into(),
                        )
                    }
                }
            }
            (InterpState::Adding(acc), MathToken::Number(number)) => InterpState::Acc(acc + number),
            (InterpState::Adding(acc), MathToken::OpenParen) => {
                state_stack.push_back(InterpState::Adding(acc));
                InterpState::Start
            }
            (InterpState::Multiplying(acc), MathToken::Number(number)) => {
                InterpState::Acc(acc * number)
            }
            (InterpState::Multiplying(acc), MathToken::OpenParen) => {
                state_stack.push_back(InterpState::Multiplying(acc));
                InterpState::Start
            }
            _ => {
                return Err(format!(
                    "Syntax error! State: {:?}; Next token: {:?}",
                    state, next_token
                )
                .into())
            }
        }
    }

    if state_stack.len() > 0 {
        return Err("Missing closing parentheses!".into());
    }

    if let InterpState::Acc(acc) = state {
        return Ok(acc);
    }

    Err("Invalid end state! Cannot end while calculating!".into())
}
