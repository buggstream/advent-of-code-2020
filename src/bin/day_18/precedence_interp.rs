use crate::MathToken;
use std::collections::VecDeque;
use std::error::Error;
use std::iter::FromIterator;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum InterpState {
    StartScope,
    Acc(i64),
    Adding(i64),
    Multiplying(i64),
    MultiplyRequest(i64, i64),
    AddPrecedence(i64),
}

impl InterpState {
    fn new_scope_allowed(&self) -> bool {
        match self {
            InterpState::StartScope => true,
            InterpState::Adding(_) => true,
            InterpState::Multiplying(_) => true,
            InterpState::AddPrecedence(_) => true,
            _ => false,
        }
    }

    fn to_value(&self) -> Option<i64> {
        match self {
            InterpState::Acc(number) => Some(*number),
            InterpState::MultiplyRequest(left, right) => Some(*left * *right),
            _ => None,
        }
    }
}

pub fn interp(tokens: &[MathToken]) -> Result<i64, Box<dyn Error>> {
    let mut token_stack = VecDeque::from_iter(tokens.iter().copied().rev());
    let mut state_stack = VecDeque::new();
    let mut state = InterpState::StartScope;

    while let Some(next_token) = token_stack.pop_back() {
        state = match (state, next_token) {
            // Number
            (InterpState::StartScope, MathToken::Number(number)) => InterpState::Acc(number),
            (InterpState::Adding(acc), MathToken::Number(number)) => InterpState::Acc(acc + number),
            (InterpState::Multiplying(acc), MathToken::Number(number)) => {
                InterpState::MultiplyRequest(acc, number)
            }
            (InterpState::AddPrecedence(acc), MathToken::Number(number)) => {
                token_stack.push_back(MathToken::Number(acc + number));
                state_stack.pop_back().expect(
                    "Can only get into precedence state if something has been pushed to the stack",
                )
            }
            // Add
            (InterpState::Acc(acc), MathToken::Add) => InterpState::Adding(acc),
            (InterpState::MultiplyRequest(acc, multiplier), MathToken::Add) => {
                state_stack.push_back(InterpState::Multiplying(acc));
                InterpState::AddPrecedence(multiplier)
            }
            // Multiply
            (InterpState::Acc(acc), MathToken::Multiply) => InterpState::Multiplying(acc),
            (InterpState::MultiplyRequest(acc, multiplier), MathToken::Multiply) => {
                InterpState::Multiplying(acc * multiplier)
            }
            // Start scope
            (_, MathToken::OpenParen) if state.new_scope_allowed() => {
                state_stack.push_back(state);
                InterpState::StartScope
            }
            // End scope
            (_, MathToken::CloseParen) => {
                match state.to_value() {
                    Some(value) => token_stack.push_back(MathToken::Number(value)),
                    None => return Err("Cannot close parentheses after operator".into()),
                }

                match state_stack.pop_back() {
                    Some(prev_state) => prev_state,
                    None => {
                        return Err(
                            "Syntax error! Closing parentheses without opening parentheses".into(),
                        )
                    }
                }
            }
            // Error handling
            _ => {
                return Err(format!(
                    "Syntax error! State: {:?}; Next token: {:?}",
                    state, next_token
                )
                .into())
            }
        }
    }

    if state_stack.len() > 0 {
        return Err("Missing closing parentheses!".into());
    }

    match state.to_value() {
        Some(acc) => Ok(acc),
        None => Err("Invalid end state! Cannot end while calculating!".into()),
    }
}
