mod left_right_interp;
mod precedence_interp;

use advent_of_code_2020::read_puzzle_input;
use std::collections::VecDeque;
use std::error::Error;
use std::iter::FromIterator;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(18)?;
    let sum = puzzle_one(&input)?;

    println!("All exercises sum: {}", sum);

    let sum = puzzle_two(&input)?;

    println!("Sum with precedences: {}", sum);

    Ok(())
}

fn puzzle_one(input: &str) -> Result<i64, Box<dyn Error>> {
    input
        .lines()
        .map(|line| left_right_interp::interp(&tokenize(line)?))
        .sum()
}

fn puzzle_two(input: &str) -> Result<i64, Box<dyn Error>> {
    input
        .lines()
        .map(|line| precedence_interp::interp(&tokenize(line)?))
        .sum()
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MathToken {
    Number(i64),
    Add,
    Multiply,
    OpenParen,
    CloseParen,
}

pub fn tokenize(input: &str) -> Result<Vec<MathToken>, Box<dyn Error>> {
    let mut parsed_tokens = Vec::new();
    let mut char_stack = VecDeque::from_iter(input.chars().rev());

    while let Some(character) = char_stack.pop_back() {
        let parsed_element = match character {
            _ if character.is_ascii_digit() => {
                let mut digits = String::new();
                digits.push(character);

                while let Some(next_char) = char_stack.pop_back() {
                    if !next_char.is_ascii_digit() {
                        char_stack.push_back(next_char);
                        break;
                    } else {
                        digits.push(next_char);
                    }
                }

                MathToken::Number(digits.parse()?)
            }
            '+' => MathToken::Add,
            '*' => MathToken::Multiply,
            '(' => MathToken::OpenParen,
            ')' => MathToken::CloseParen,
            _ => continue,
        };

        parsed_tokens.push(parsed_element);
    }

    Ok(parsed_tokens)
}

#[cfg(test)]
mod test {
    use crate::{left_right_interp, precedence_interp, tokenize};
    use std::error::Error;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = "1 + (2 * 3) + (4 * (5 + 6))";
        let tokens = tokenize(input)?;
        let answer = left_right_interp::interp(&tokens)?;

        assert_eq!(51, answer);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = "2 * 3 + (4 * 5)";
        let tokens = tokenize(input)?;
        let answer = precedence_interp::interp(&tokens)?;

        assert_eq!(46, answer);

        Ok(())
    }
}
