use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

pub fn read_puzzle_input(day: u32) -> Result<String, Box<dyn Error>> {
    let file_name = format!("inputs/day_{}", day);
    let mut file_contents = String::new();
    File::open(&file_name)?.read_to_string(&mut file_contents)?;
    Ok(file_contents)
}

pub trait ParseList: FromStr {
    fn parse_list(input: &str) -> Result<Vec<Self>, Self::Err> {
        input.lines().map(|line| FromStr::from_str(line)).collect()
    }
}

impl ParseList for u64 {}
impl ParseList for u32 {}

impl ParseList for i64 {}
impl ParseList for i32 {}
