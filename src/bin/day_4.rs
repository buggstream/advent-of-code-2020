use advent_of_code_2020::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;

lazy_static! {
    static ref FOUR_DIGITS: Regex = Regex::new(r"^\d{4}$").unwrap();
    static ref HEIGHT: Regex = Regex::new(r"^(\d+)([[:alpha:]]+)$").unwrap();
    static ref HAIR_COLOR: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    static ref PASSPORT_ID: Regex = Regex::new(r"^\d{9}$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(4)?;
    let passports = parse_passports(&input);
    let valid_passports = puzzle_one(&passports);

    println!("{}", valid_passports);

    let verified_passports = puzzle_two(&passports);
    println!("{}", verified_passports);

    Ok(())
}

fn puzzle_one(passports: &[HashMap<&str, &str>]) -> u32 {
    passports
        .iter()
        .filter(|passport| {
            ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
                .iter()
                .all(|field| passport.contains_key(*field))
        })
        .count() as u32
}

fn puzzle_two(passports: &[HashMap<&str, &str>]) -> u32 {
    let mut valid = 0;
    let required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

    for passport in passports {
        let all_required_present = required_fields
            .iter()
            .all(|field| passport.contains_key(*field));

        if !all_required_present {
            continue;
        }

        let valid_fields = passport.iter().all(|(key, value)| match *key {
            "byr" => match FOUR_DIGITS.find(value) {
                Some(matched) => {
                    number_between_incl(matched.as_str().parse::<u32>().unwrap(), 1920, 2002)
                }
                None => false,
            },
            "iyr" => match FOUR_DIGITS.find(value) {
                Some(matched) => {
                    number_between_incl(matched.as_str().parse::<u32>().unwrap(), 2010, 2020)
                }
                None => false,
            },
            "eyr" => match FOUR_DIGITS.find(value) {
                Some(matched) => {
                    number_between_incl(matched.as_str().parse::<u32>().unwrap(), 2020, 2030)
                }
                None => false,
            },
            "hgt" => match HEIGHT.captures(value) {
                Some(captures) => {
                    let height_str = captures.get(1).unwrap().as_str();
                    let unit_str = captures.get(2).unwrap().as_str();

                    match (unit_str, height_str.parse::<u32>().ok()) {
                        ("cm", Some(height)) => number_between_incl(height, 150, 193),
                        ("in", Some(height)) => number_between_incl(height, 59, 76),
                        _ => false,
                    }
                }
                None => false,
            },
            "hcl" => HAIR_COLOR.is_match(value),
            "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(value),
            "pid" => PASSPORT_ID.is_match(value),
            _ => true,
        });

        if valid_fields {
            valid += 1;
        }
    }

    valid
}

fn number_between_incl(number: u32, lower: u32, upper: u32) -> bool {
    number >= lower && number <= upper
}

fn parse_passports(input: &str) -> Vec<HashMap<&str, &str>> {
    input
        .split("\n\n")
        .map(|passport| {
            passport
                .split_whitespace()
                .map(|key_value| {
                    let split = key_value.find(":").unwrap();
                    (&key_value[..split], &key_value[split + 1..])
                })
                .collect()
        })
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{parse_passports, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_two_valid() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_4_valid")?;
        let passports = parse_passports(&input);
        let valid_passports = puzzle_two(&passports);

        assert_eq!(4, valid_passports);

        Ok(())
    }

    #[test]
    fn puzzle_two_invalid() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_4_invalid")?;
        let passports = parse_passports(&input);
        let valid_passports = puzzle_two(&passports);

        assert_eq!(0, valid_passports);

        Ok(())
    }
}
