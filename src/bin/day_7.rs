use advent_of_code_2020::read_puzzle_input;
use lazy_static::lazy_static;
use petgraph::graph::DiGraph;
use petgraph::visit::EdgeRef;
use petgraph::Direction;
use regex::Regex;
use std::collections::{HashMap, HashSet, VecDeque};
use std::error::Error;

lazy_static! {
    static ref SPLIT_LINE: Regex = Regex::new(r"(?m)^(.+) bags contain (.+)$").unwrap();
    static ref DEPENDENT_BAGS: Regex = Regex::new(r"(\d+) ([^\.,]+) bags?").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(7)?;
    let graph = parse_bags(&input);
    let bags_outside = puzzle_one(&graph);
    println!("{}", bags_outside);

    let bags_inside = puzzle_two(&graph);
    println!("{}", bags_inside);

    Ok(())
}

fn puzzle_one(graph: &DiGraph<&str, u64>) -> usize {
    let my_bag = graph
        .node_indices()
        .find(|node| *graph.node_weight(*node).unwrap() == "shiny gold")
        .unwrap();

    let mut explored = HashSet::new();
    let mut frontier = VecDeque::new();
    frontier.push_back(my_bag);

    while let Some(node_index) = frontier.pop_back() {
        if !explored.insert(node_index) {
            continue;
        }

        for edge in graph.edges_directed(node_index, Direction::Incoming) {
            let (source, _) = graph.edge_endpoints(edge.id()).unwrap();
            frontier.push_back(source);
        }
    }

    explored.len() - 1
}

fn puzzle_two(graph: &DiGraph<&str, u64>) -> u64 {
    let my_bag = graph
        .node_indices()
        .find(|node| *graph.node_weight(*node).unwrap() == "shiny gold")
        .unwrap();

    let mut total_needed = 0;
    let mut frontier = VecDeque::new();
    frontier.push_back((my_bag, 1));

    while let Some((node_index, amount_needed)) = frontier.pop_back() {
        total_needed += amount_needed;

        for edge in graph.edges_directed(node_index, Direction::Outgoing) {
            let (_, to) = graph.edge_endpoints(edge.id()).unwrap();
            frontier.push_back((to, *edge.weight() * amount_needed));
        }
    }

    total_needed - 1
}

fn parse_bags(input: &str) -> DiGraph<&str, u64> {
    let mut graph = DiGraph::new();
    let mut node_indices = HashMap::new();

    for captures in SPLIT_LINE.captures_iter(input) {
        let super_bag = captures.get(1).unwrap().as_str();
        let super_node = *node_indices
            .entry(super_bag)
            .or_insert_with(|| graph.add_node(super_bag));
        let sub_bags_str = captures.get(2).unwrap().as_str();

        for sub_bag_captures in DEPENDENT_BAGS.captures_iter(sub_bags_str) {
            let sub_bag_count = sub_bag_captures[1].parse::<u64>().unwrap();
            let sub_bag_id = sub_bag_captures.get(2).unwrap().as_str();
            let sub_node = *node_indices
                .entry(sub_bag_id)
                .or_insert_with(|| graph.add_node(sub_bag_id));

            graph.add_edge(super_node, sub_node, sub_bag_count);
        }
    }

    graph
}

#[cfg(test)]
mod test {
    use crate::{parse_bags, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_7_example")?;
        let graph = parse_bags(&input);
        let bags_outside = puzzle_one(&graph);

        assert_eq!(4, bags_outside);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_7_example")?;
        let graph = parse_bags(&input);
        let bags_inside = puzzle_two(&graph);

        assert_eq!(32, bags_inside);

        Ok(())
    }

    #[test]
    fn puzzle_two_tree() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_7_binary_tree")?;
        let graph = parse_bags(&input);
        let bags_inside = puzzle_two(&graph);

        assert_eq!(126, bags_inside);

        Ok(())
    }
}
