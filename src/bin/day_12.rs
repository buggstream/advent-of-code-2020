use advent_of_code_2020::{read_puzzle_input, ParseList};
use parse_display::{Display as PDisplay, FromStr as PFromStr};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(12)?;
    let instructions = NavigationInstruction::parse_list(&input)?;
    let distance = puzzle_one(&instructions);

    println!("{}", distance);

    let distance_waypoint_nav = puzzle_two(&instructions);

    println!("{}", distance_waypoint_nav);

    Ok(())
}

fn puzzle_one(instructions: &[NavigationInstruction]) -> i64 {
    let mut ship = Ship::default();
    ship.follow_simple_instructions(instructions);
    ship.manhattan_distance()
}

fn puzzle_two(instructions: &[NavigationInstruction]) -> i64 {
    let mut ship = Ship::default();
    ship.follow_waypoint_instructions(instructions);
    ship.manhattan_distance()
}

#[derive(Debug, Copy, Clone)]
struct Ship {
    x: i64,
    y: i64,
    facing: Direction,
    waypoint_x: i64,
    waypoint_y: i64,
}

impl Ship {
    fn follow_simple_instructions(&mut self, instructions: &[NavigationInstruction]) {
        for instruction in instructions {
            match instruction {
                NavigationInstruction::North(amount) => self.y += *amount,
                NavigationInstruction::East(amount) => self.x += *amount,
                NavigationInstruction::South(amount) => self.y -= *amount,
                NavigationInstruction::West(amount) => self.x -= *amount,
                NavigationInstruction::Left(degrees) => {
                    self.facing = self.facing.turn_degrees(*degrees)
                }
                NavigationInstruction::Right(degrees) => {
                    self.facing = self.facing.turn_degrees(-*degrees)
                }
                NavigationInstruction::Forward(amount) => match self.facing {
                    Direction::North => self.y += *amount,
                    Direction::East => self.x += *amount,
                    Direction::South => self.y -= *amount,
                    Direction::West => self.x -= *amount,
                },
            }
        }
    }

    fn follow_waypoint_instructions(&mut self, instructions: &[NavigationInstruction]) {
        for instruction in instructions {
            match instruction {
                NavigationInstruction::North(amount) => self.waypoint_y += *amount,
                NavigationInstruction::East(amount) => self.waypoint_x += *amount,
                NavigationInstruction::South(amount) => self.waypoint_y -= *amount,
                NavigationInstruction::West(amount) => self.waypoint_x -= *amount,
                NavigationInstruction::Left(degrees) => self.rotate_waypoint(*degrees),
                NavigationInstruction::Right(degrees) => self.rotate_waypoint(-*degrees),
                NavigationInstruction::Forward(amount) => {
                    self.x += self.waypoint_x * *amount;
                    self.y += self.waypoint_y * *amount;
                }
            }
        }
    }

    fn rotate_waypoint(&mut self, degrees: i64) {
        let degrees = ((degrees % 360) + 360) % 360;
        if degrees % 90 > 0 {
            panic!("Invalid degrees amount for waypoint rotation!");
        }

        let rotations = degrees / 90;

        for _ in 0..rotations {
            let new_waypoint_x = -self.waypoint_y;
            let new_waypoint_y = self.waypoint_x;

            self.waypoint_x = new_waypoint_x;
            self.waypoint_y = new_waypoint_y;
        }
    }

    fn manhattan_distance(&self) -> i64 {
        self.x.abs() + self.y.abs()
    }
}

impl Default for Ship {
    fn default() -> Self {
        Ship {
            x: 0,
            y: 0,
            facing: Direction::East,
            waypoint_x: 10,
            waypoint_y: 1,
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn turn_degrees(&self, degrees: i64) -> Direction {
        let degrees = ((degrees % 360) + 360) % 360;
        if degrees % 90 > 0 {
            panic!("Invalid degrees amount!");
        }

        let rotations = degrees / 90;
        let mut new_direction = *self;

        for _ in 0..rotations {
            match new_direction {
                Direction::North => new_direction = Direction::West,
                Direction::East => new_direction = Direction::North,
                Direction::South => new_direction = Direction::East,
                Direction::West => new_direction = Direction::South,
            }
        }

        new_direction
    }
}

#[derive(PDisplay, PFromStr, Debug, Copy, Clone)]
enum NavigationInstruction {
    #[display("N{0}")]
    North(i64),
    #[display("E{0}")]
    East(i64),
    #[display("S{0}")]
    South(i64),
    #[display("W{0}")]
    West(i64),
    #[display("L{0}")]
    Left(i64),
    #[display("R{0}")]
    Right(i64),
    #[display("F{0}")]
    Forward(i64),
}

impl ParseList for NavigationInstruction {}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two, NavigationInstruction};
    use advent_of_code_2020::ParseList;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_12_small")?;
        let instructions = NavigationInstruction::parse_list(&input)?;
        let distance = puzzle_one(&instructions);

        assert_eq!(25, distance);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_12_small")?;
        let instructions = NavigationInstruction::parse_list(&input)?;
        let distance = puzzle_two(&instructions);

        assert_eq!(286, distance);

        Ok(())
    }
}
