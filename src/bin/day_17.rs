use advent_of_code_2020::read_puzzle_input;
use itertools::{repeat_n, Itertools};
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::RangeInclusive;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(17)?;
    let mut cube_simulation = CubeSimulation::<3>::from_str(&input)?;
    let active_cubes = cube_simulation.run_simulation::<false>(6);

    println!("{}", active_cubes);

    let mut cube_simulation = CubeSimulation::<4>::from_str(&input)?;
    let active_cubes = cube_simulation.run_simulation::<false>(6);

    println!("{}", active_cubes);

    let mut cube_simulation = CubeSimulation::<5>::from_str(&input)?;
    let active_cubes = cube_simulation.run_simulation::<false>(6);

    println!("{}", active_cubes);

    Ok(())
}

#[derive(Debug, Clone)]
struct CubeSimulation<const N: usize> {
    active_cubes: HashSet<[i64; N]>,
}

impl<const N: usize> CubeSimulation<N> {
    fn run_simulation<const DEBUG: bool>(&mut self, iterations: u32) -> usize {
        for step in 1..=iterations {
            self.simulation_step();
            if DEBUG {
                println!("Step {}:\n{}", step, self);
            }
        }

        self.active_cubes.len()
    }

    fn simulation_step(&mut self) {
        let mut activation_sums = HashMap::new();

        for coord in self.active_cubes.iter().copied() {
            for neighbour in neighbours(coord) {
                let sum = activation_sums.entry(neighbour).or_insert(0_u8);
                *sum += 1;
            }
        }

        self.active_cubes
            .retain(|coord| match activation_sums.get(coord) {
                Some(sum) => (2..=3).contains(sum),
                _ => false,
            });

        self.active_cubes.extend(
            activation_sums
                .iter()
                .filter(|(_, sum)| **sum == 3)
                .map(|(coord, _)| *coord),
        );
    }
}

fn array_from_slice<const N: usize>(slice: &[i64]) -> [i64; N] {
    let mut array = [0_i64; N];
    array.as_mut()[..slice.len()].copy_from_slice(slice);
    array
}

fn neighbours<const N: usize>(coord: [i64; N]) -> impl Iterator<Item = [i64; N]> {
    repeat_n(-1..=1, coord.as_ref().len())
        .multi_cartesian_product()
        .filter(|vec| !vec.iter().all(|element| *element == 0))
        .map(move |mut vec| {
            vec.iter_mut()
                .enumerate()
                .for_each(|(index, element)| *element += coord.as_ref()[index]);
            vec
        })
        .map(|vec| array_from_slice(&vec))
}

impl<const N: usize> FromStr for CubeSimulation<N> {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut active_cubes = HashSet::new();

        for (y, line) in input.lines().enumerate() {
            for (x, state) in line.chars().enumerate() {
                if state == '#' {
                    let cube = array_from_slice([x as i64, y as i64].as_ref());
                    active_cubes.insert(cube);
                }
            }
        }

        Ok(CubeSimulation { active_cubes })
    }
}

fn bounding_box_3d<'a, 'b>(
    mut iter: impl Iterator<Item = &'b [i64]> + 'a,
) -> Option<[RangeInclusive<i64>; 3]> {
    let mut min_coord = match iter.next() {
        Some([x, y, z, ..]) => [*x, *y, *z],
        _ => return None,
    };

    let mut max_coord = min_coord;

    while let Some(cur_coord @ [_, _, _, ..]) = iter.next() {
        for index in 0..min_coord.len() {
            min_coord[index] = min(min_coord[index], cur_coord[index]);
            max_coord[index] = max(max_coord[index], cur_coord[index]);
        }
    }

    let [min_x, min_y, min_z] = min_coord;
    let [max_x, max_y, max_z] = max_coord;

    Some([min_x..=max_x, min_y..=max_y, min_z..=max_z])
}

impl<const N: usize> Display for CubeSimulation<N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let [bounding_x, bounding_y, bounding_z] =
            match bounding_box_3d(self.active_cubes.iter().map(|array| array.as_ref())) {
                Some(bounding_boxes) => bounding_boxes,
                None => return Ok(()),
            };

        for z in bounding_z {
            write!(f, "--------\nz = {}\n", z)?;

            for y in bounding_y.clone() {
                for x in bounding_x.clone() {
                    let cube = array_from_slice([x, y, z].as_ref());
                    match self.active_cubes.contains(&cube) {
                        true => write!(f, "#")?,
                        false => write!(f, ".")?,
                    };
                }

                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::CubeSimulation;
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_17_small")?;
        let mut cube_simulation = CubeSimulation::<3>::from_str(&input)?;
        let active_cubes = cube_simulation.run_simulation::<false>(6);

        assert_eq!(112, active_cubes);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_17_small")?;
        let mut cube_simulation = CubeSimulation::<4>::from_str(&input)?;
        let active_cubes = cube_simulation.run_simulation::<false>(6);

        assert_eq!(848, active_cubes);

        Ok(())
    }
}
