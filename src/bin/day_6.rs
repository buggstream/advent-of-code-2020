use advent_of_code_2020::read_puzzle_input;
use std::collections::HashSet;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(6)?;
    let groups = parse_groups(&input);

    let unique_per_group = puzzle_one(&groups);
    println!("{}", unique_per_group);

    let intersection_per_group = puzzle_two(&groups);
    println!("{}", intersection_per_group);

    Ok(())
}

fn puzzle_one(groups: &[Vec<HashSet<char>>]) -> u64 {
    groups
        .iter()
        .map(|group| {
            group
                .iter()
                .flatten()
                .copied()
                .collect::<HashSet<char>>()
                .len() as u64
        })
        .sum()
}

fn puzzle_two(groups: &[Vec<HashSet<char>>]) -> u64 {
    groups
        .iter()
        .map(|group| {
            let mut iter = group.into_iter();
            let first = iter.next().cloned().unwrap_or(HashSet::new());
            iter.fold(first, |combined, person| {
                combined.intersection(person).copied().collect()
            })
        })
        .map(|group_answers| group_answers.len() as u64)
        .sum()
}

fn parse_groups(input: &str) -> Vec<Vec<HashSet<char>>> {
    input
        .split("\n\n")
        .map(|group| group.lines().map(|line| line.chars().collect()).collect())
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{parse_groups, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_6_example")?;
        let groups = parse_groups(&input);
        let unique_per_group = puzzle_one(&groups);

        assert_eq!(11, unique_per_group);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_6_example")?;
        let groups = parse_groups(&input);
        let intersection_per_group = puzzle_two(&groups);

        assert_eq!(6, intersection_per_group);

        Ok(())
    }
}
