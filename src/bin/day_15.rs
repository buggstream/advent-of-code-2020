use advent_of_code_2020::read_puzzle_input;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(15)?;
    let numbers = parse_numbers(&input)?;
    let number_spoken = memory_game(&numbers, 2020);

    println!("{:?}", number_spoken);

    let number_spoken = memory_game(&numbers, 30000000);

    println!("{:?}", number_spoken);

    Ok(())
}

fn memory_game(starting_numbers: &[usize], iterations: usize) -> Option<usize> {
    let mut last_spoken = *starting_numbers.last()?;
    let mut memory = vec![0; iterations];

    for (turn, starting_number) in starting_numbers.iter().copied().enumerate() {
        memory[starting_number] = turn + 1;
    }

    for turn in starting_numbers.len()..iterations {
        let next_spoken = if memory[last_spoken] > 0 {
            turn - memory[last_spoken]
        } else {
            0
        };

        memory[last_spoken] = turn;
        last_spoken = next_spoken;
    }

    Some(last_spoken)
}

fn parse_numbers(input: &str) -> Result<Vec<usize>, impl Error> {
    input
        .trim()
        .split(',')
        .map(|number| number.parse::<usize>())
        .collect::<Result<Vec<_>, _>>()
}

#[cfg(test)]
mod test {
    use crate::{memory_game, parse_numbers};
    use std::error::Error;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = "0,3,6";
        let numbers = parse_numbers(input)?;
        let number_spoken = memory_game(&numbers, 2020);

        assert_eq!(Some(436), number_spoken);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = "0,3,6";
        let numbers = parse_numbers(input)?;
        let number_spoken = memory_game(&numbers, 30000000);

        assert_eq!(Some(175594), number_spoken);

        Ok(())
    }
}
