use advent_of_code_2020::read_puzzle_input;
use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(3)?;
    let tree_map = TreeMap::from_str(&input)?;
    let trees_encountered = puzzle_one(&tree_map);

    println!("{}", trees_encountered);

    let trees_multiplied = puzzle_two(&tree_map);

    println!("{}", trees_multiplied);

    Ok(())
}

fn puzzle_one(tree_map: &TreeMap) -> u32 {
    trees_encountered(tree_map, (3, 1))
}

fn puzzle_two(tree_map: &TreeMap) -> u32 {
    [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|direction| trees_encountered(tree_map, *direction))
        .product()
}

fn trees_encountered(tree_map: &TreeMap, (x_dir, y_dir): (usize, usize)) -> u32 {
    let mut trees_encountered = 0;

    let (mut x, mut y) = (0, 0);

    loop {
        x += x_dir;
        y += y_dir;
        let current_tile = tree_map.get_tile(x, y);

        match current_tile {
            Some(Tile::Tree) => trees_encountered += 1,
            Some(Tile::Open) => (),
            None => break,
        }
    }

    trees_encountered
}

#[derive(Debug)]
struct TreeMap {
    tiles: Vec<Vec<Tile>>,
    width: usize,
    height: usize,
}

impl TreeMap {
    fn get_tile(&self, x: usize, y: usize) -> Option<&Tile> {
        self.tiles
            .get(y)
            .map(|row| row.get(x % self.width).unwrap())
    }
}

impl FromStr for TreeMap {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rows = Vec::new();
        let mut width = None;

        for line in s.lines() {
            if width.is_none() {
                width = Some(line.chars().count())
            } else if Some(line.chars().count()) != width {
                return Err("Amount of characters do not match!".into());
            }

            let tile_row: Result<Vec<_>, _> =
                line.chars().map(|letter| letter.try_into()).collect();
            rows.push(tile_row?);
        }

        Ok(TreeMap {
            height: rows.len(),
            tiles: rows,
            width: width.unwrap_or(0),
        })
    }
}

#[derive(Debug)]
enum Tile {
    Open,
    Tree,
}

impl TryFrom<char> for Tile {
    type Error = Box<dyn Error>;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Tile::Open),
            '#' => Ok(Tile::Tree),
            _ => Err("Invalid tile character".into()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two, TreeMap};
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_test() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_3_small")?;
        let tree_map = TreeMap::from_str(&input)?;
        let solution = puzzle_one(&tree_map);

        assert_eq!(7, solution);

        Ok(())
    }

    #[test]
    fn puzzle_two_test() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/extra-test-cases/day_3_small")?;
        let tree_map = TreeMap::from_str(&input)?;
        let solution = puzzle_two(&tree_map);

        assert_eq!(336, solution);

        Ok(())
    }
}
